﻿using UnityEngine;

namespace HG.LevelObjects.View
{
    public class RoomView : MonoBehaviour
    {
        [SerializeField] SpriteRenderer sprite;

        public void SetUp(Vector2 size)
        {
            sprite.transform.localScale = new Vector3(size.x, size.y, 1);
        }
    }
}
