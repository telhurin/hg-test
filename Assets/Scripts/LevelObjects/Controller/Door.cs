﻿using HG.LevelObjects.View;
using UnityEngine;

namespace HG.LevelObjects.Controller
{
    public class Door : MonoBehaviour
    {
        [SerializeField] DoorView view;

        public Vector2 Center => transform.position;

        public void SetUp(Vector2 position, Vector2 size)
        {
            SetUpPosition(position);
            SetUpView(size);
        }

        private void SetUpPosition(Vector2 position)
        {
            transform.position = position;
        }

        private void SetUpView(Vector2 size)
        {
            view.SetUp(size);
        }

        private void Reset()
        {
            view = GetComponent<DoorView>();
        }
    }

}
