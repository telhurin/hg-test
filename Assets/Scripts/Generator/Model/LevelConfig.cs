﻿using UnityEngine;

namespace HG.Generator.Model
{
    [CreateAssetMenu(menuName = "HG/Configs/Level Config")]
    public class LevelConfig : ScriptableObject
    {
        public float doorSize = 1;
        public float roomSize = 3;
        public int maxRooms = 10;
        public int maxExits = 4;

        [Range(1, 4)]
        public int maxConnectionsPerRoom = 2;
    }
}
