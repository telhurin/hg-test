﻿using HG.Generator.Model;
using HG.LevelObjects.Controller;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Random = System.Random;

namespace HG.Generator.Controller
{
    public class LevelGenerator : MonoBehaviour
    {
        public static readonly Vector2[] Directions = new Vector2[]
        {
            new Vector2(-1, 0),
            new Vector2(1, 0),
            new Vector2(0, -1),
            new Vector2(0, 1),
        };

        [SerializeField] bool useRandomSeed;
        [SerializeField] int forcedSeed = 0;
        [SerializeField] int maxIterations = 99;

        [Header("Configs")]
        [SerializeField] LevelConfig config;

        [Header("Prefabs")]
        [SerializeField] Room roomPrefab;
        [SerializeField] Door doorPrefab;
        [SerializeField] Door exitPrefab;

        private Transform levelParent;
        private Random rng;

        private List<Room> rooms;
        private List<Door> doors;
        private List<Door> exits;

        private void Awake()
        {
            RegenerateLevel();
        }

        [ContextMenu("Regenerate")]
        private void RegenerateLevel()
        {
            SetUpRandomGenerator();
            SetUpParent();
            PrepareList();
            GenerateRooms();
            GenerateExits();
        }

        private void SetUpParent()
        {
            if (levelParent != null)
                Destroy(levelParent.gameObject);

            levelParent = new GameObject("Level").transform;
        }

        private void SetUpRandomGenerator()
        {
            rng = useRandomSeed ? new Random() : new Random(forcedSeed);
        }

        private void PrepareList()
        {
            rooms = new List<Room>();
            doors = new List<Door>();
            exits = new List<Door>();
        }

        private void GenerateRooms()
        {
            SpawnRoom(Vector2.zero);
            for (int i = 0; i < maxIterations && ShouldAddMoreRooms(); i++)
                ExecuteIteration();
        }

        private void ExecuteIteration()
        {
            for (int i = rooms.Count - 1; i >= 0; i--)
                if (ShouldAddMoreRooms())
                    TryToAddExtraRoom(rooms[i]);
        }

        private bool ShouldAddMoreRooms()
        {
            return rooms.Count < config.maxRooms;
        }

        private void GenerateExits()
        {
            for (int i = rooms.Count - 1; i >= 0 && exits.Count < config.maxExits; i--)
                if (HaveSocketForExit(rooms[i]))
                    AddExit(rooms[i]);
        }

        private void SpawnRoom(Vector2 position)
        {
            Assert.IsNotNull(roomPrefab, "Room prefab is not attached in the inspector.");

            var room = Instantiate(roomPrefab, levelParent);
            room.SetUp(position, new Vector2(config.roomSize, config.roomSize));
            rooms.Add(room);
        }

        private void SpawnDoor(Vector2 position)
        {
            Assert.IsNotNull(doorPrefab, "Door prefab is not attached in the inspector.");

            var door = Instantiate(doorPrefab, levelParent);
            door.SetUp(position, new Vector2(config.doorSize, config.doorSize));
            doors.Add(door);
        }

        private void SpawnExit(Vector2 position)
        {
            Assert.IsNotNull(doorPrefab, "Exit prefab is not attached in the inspector.");

            var exit = Instantiate(exitPrefab, levelParent);
            exit.SetUp(position, new Vector2(config.doorSize, config.doorSize));
            exits.Add(exit);
        }

        private void TryToAddExtraRoom(Room room)
        {
            if (!HaveSocketForRoom(room))
                return;

            int randomStart = rng.Next(0, Directions.Length);

            for (int i = 0; i < Directions.Length; i++)
            {
                var dir = Directions[(i + randomStart) % Directions.Length];
                if (CanAttachTo(room, dir))
                {
                    SpawnNewRoom(room, dir);
                    return;
                }
            }
        }

        private void SpawnNewRoom(Room roomFrom, Vector2 dir)
        {
            SpawnRoom(GetNewRoomPosition(roomFrom, dir));
            SpawnDoor(GetNewDoorPosition(roomFrom, dir));

            roomFrom.AddConnection();
        }

        private bool CanAttachTo(Room roomFrom, Vector2 dir)
        {
            var newRoomPos = GetNewRoomPosition(roomFrom, dir);
            var newRoomRect = CreateRect(newRoomPos, config.roomSize);

            foreach (var room in rooms)
                if (CreateRect(room.Center, config.roomSize).Overlaps(newRoomRect))
                    return false;

            return true;
        }

        private void AddExit(Room roomFrom)
        {
            int randomStart = rng.Next(0, Directions.Length);

            for (int i = 0; i < Directions.Length; i++)
            {
                var dir = Directions[(i + randomStart) % Directions.Length];
                if (CanCreateExit(roomFrom, dir))
                {
                    SpawnExit(GetNewDoorPosition(roomFrom, dir));
                    return;
                }
            }
        }

        private bool CanCreateExit(Room roomFrom, Vector2 dir)
        {
            var newExitPos = GetNewDoorPosition(roomFrom, dir);
            var newDoorRect = CreateRect(newExitPos, config.doorSize);

            foreach (var door in doors)
                if (CreateRect(door.Center, config.doorSize).Overlaps(newDoorRect))
                    return false;

            return true;
        }

        private Vector2 GetNewRoomPosition(Room roomFrom, Vector2 dir)
        {
            return roomFrom.Center + (config.roomSize * dir) + (config.doorSize * dir);
        }

        private Vector2 GetNewDoorPosition(Room roomFrom, Vector2 dir)
        {
            return roomFrom.Center + ((config.roomSize / 2f) * dir) + ((config.doorSize / 2f) * dir);
        }

        private Rect GetRectWithOffsetFor(Room room)
        {
            return new Rect(room.Center, new Vector2(config.roomSize, config.roomSize));
        }

        private bool HaveSocketForRoom(Room room)
        {
            return room.CountConnections() < config.maxConnectionsPerRoom;
        }

        private bool HaveSocketForExit(Room room)
        {
            return room.CountConnections() < Directions.Length;
        }

        private Rect CreateRect(Vector2 newRoomPos, float size)
        {
            return new Rect(newRoomPos, new Vector2(size, size));
        }
    }
}
